/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef INCLUDE_WLD_WLD_VENDORMODULE_H_
#define INCLUDE_WLD_WLD_VENDORMODULE_H_

#include <amxc/amxc_variant.h>
#include <amxm/amxm.h>
#include "swl/swl_returnCode.h"

/**
 * @brief required string prefix in the vendor module shared object file
 */
#define WLD_VENDOR_MODULE_PREFIX "mod-"

/**
 * @brief data struct to share init info between wld and vendor module
 */
typedef struct {
} wld_vendorModule_initInfo_t;

/**
 * @brief Prototype of vendor module initialization function.
 * This handler is called after wld initialization
 *
 * @param pInitInfo pointer for shared initialization info
 *
 * @return true on success, false on failure
 */
typedef bool (* wld_vendorModule_init_f) (wld_vendorModule_initInfo_t* pInitInfo);

/**
 * @brief Prototype of vendor module de-initialization function.
 *
 * @return true on success, false on failure
 */
typedef bool (* wld_vendorModule_deinit_f) ();

/**
 * @brief Prototype of vendor module function to load default vendor configuration.
 *
 * @return true on success, false on failure
 */
typedef bool (* wld_vendorModule_loadDefaults_f) ();

/**
 * @brief Prototype of vendor module function to get vendor informations.
 * @param ret informations data, could contains the following parameters :
 *  module-name: string: optional module name (mod-whm-sah, ...)
 *  module-provider: string module provider name (sah, mxl, qcm..)
 *  module-ver: string: module release version
 *  hw-agnostic: bool : flag to indicate that module is hardware agnostic or hardware dependent
 * @return true on success, false on failure
 */
typedef bool (* wld_vendorModule_getInfo_f) (amxc_var_t* ret);

typedef struct {
    wld_vendorModule_init_f fInitCb;                 // MANDATORY: handler to initialize vendor module
    wld_vendorModule_deinit_f fDeinitCb;             // MANDATORY: handler to deinitialize vendor module
    wld_vendorModule_loadDefaults_f fLoadDefaultsCb; // OPTIONAL: handler to load default vendor datamodel config (if any)
    wld_vendorModule_getInfo_f fGetInfoCb;           // OPTIONAL: handler to get information from vendor module
} wld_vendorModule_handlers_cb;

typedef struct {
    /*
     * MANDATORY: global handler getting args in amx types
     * Then, it is up to vendor module to require forwarding amx func call
     * to vendor module specific handlers, defined here above.
     */
    amxm_callback_t fGlobalCb;
} wld_vendorModule_config_t;

/**
 * @brief Register vendor module to the wld plugin side.
 * Once done, the module handlers may be called when the wld is initialized
 *
 * @param modName Vendor module name (unique per vendor)
 * @param pConfig Pointer to vendor module configuration including the handlers
 * to be called by wld plugin side
 *
 * @return int SWL_RC_OK on success, or one of swl error codes on failure
 */
swl_rc_ne wld_vendorModule_register(const char* modName, const wld_vendorModule_config_t* const pConfig);

/**
 * @brief Unregister vendor module from the wld plugin side.
 * Once done, the module handlers are no more called.
 *
 * @param modName Vendor module name (unique per vendor)
 *
 * @return int SWL_RC_OK on success, or one of wld error codes on failure
 */
swl_rc_ne wld_vendorModule_unregister(const char* modName);

/**
 * @brief Forward function calls to vendor module handlers:
 * - convert arguments from amxm format
 * - execute the real vendor module handler mapped to function name,
 *   but with the expected argument types
 * - convert results back to amxm format
 *
 * @param function_name amxm function name called by wld
 * @param args arguments in amx variant type
 * @param ret Returned result in in amx variant type
 * @param handlers Module's internal callbacks
 *
 * @return SWL_RC_OK on success, or one of swl error codes on failure
 */
swl_rc_ne wld_vendorModule_forwardCall(const char* const funcName, amxc_var_t* args, amxc_var_t* ret,
                                       const wld_vendorModule_handlers_cb* const handlers);

/**
 * @brief Load vendor module odl file path into the main datamodel
 * using the global wld parser
 *
 * @param path vendor module odl(s) path (od file or directory of odl files)
 *             May be absolute or relative (regarding inc-dirs of pwhm plugin)
 *
 * @return SWL_RC_OK on success, or one of swl error codes on failure
 */
swl_rc_ne wld_vendorModule_loadOdls(const char* path);

/**
 * @brief Parse vendor module odl file and load into the main datamodel
 * using the global wld parser
 *
 * @param path absolute vendor module odl path (or relative to directory $cfg-dir}/${name}/)
 *
 * @return SWL_RC_OK on success, or one of swl error codes on failure
 */
swl_rc_ne wld_vendorModule_parseOdl(const char* path);

/**
 * @brief Load additional trace zones (with name and log level) to main wld logging config
 *
 * @param traceZonesVarMap pointer to htable variant including vendor module tracing zones and levels
 *
 * @return SWL_RC_OK on success, or one of swl error codes on failure
 */
swl_rc_ne wld_vendorModule_loadPrivTraceZones(amxc_var_t* traceZonesVarMap);

#endif /* INCLUDE_WLD_WLD_VENDORMODULE_H_ */
