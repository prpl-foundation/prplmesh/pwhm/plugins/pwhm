/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef INCLUDE_PRIV_PLUGIN_WIFIGEN_HAPD_H_
#define INCLUDE_PRIV_PLUGIN_WIFIGEN_HAPD_H_

#include "wld/wld.h"

#define HOSTAPD_CMD "hostapd"

swl_rc_ne wifiGen_hapd_init(T_Radio* pRad);
swl_rc_ne wifiGen_hapd_initVAP(T_AccessPoint* pAP);
void wifiGen_hapd_cleanup(T_Radio* pRad);
swl_rc_ne wifiGen_hapd_startDaemon(T_Radio* pRad);
swl_rc_ne wifiGen_hapd_stopDaemon(T_Radio* pRad);
swl_rc_ne wifiGen_hapd_reloadDaemon(T_Radio* pRad);
void wifiGen_hapd_writeConfig(T_Radio* pRad);
bool wifiGen_hapd_isRunning(T_Radio* pRad);
bool wifiGen_hapd_isAlive(T_Radio* pRad);
swl_rc_ne wifiGen_hapd_getRadState(T_Radio* pRad, chanmgt_rad_state* pDetailedState);
swl_rc_ne wifiGen_hapd_syncVapStates(T_Radio* pRad);
uint32_t wifiGen_hapd_countGrpMembers(T_Radio* pRad);
swl_rc_ne wifiGen_hapd_setGlobDmnSettings(vendor_t* pVdr, wld_dmnMgt_dmnExecSettings_t* pCfg);
swl_rc_ne wifiGen_hapd_initGlobDmnCap(T_Radio* pRad);
bool wifiGen_hapd_isStarted(T_Radio* pRad);
bool wifiGen_hapd_isStartable(T_Radio* pRad);
void wifiGen_hapd_restoreMainIface(T_Radio* pRad);
void wifiGen_hapd_enableVapWpaCtrlIface(T_AccessPoint* pAP);
bool wifiGen_hapd_parseSockName(const char* sockName, char* linkName, size_t linkNameSize, int32_t* pLinkId);
T_AccessPoint* wifiGen_hapd_fetchSockApLink(T_AccessPoint* pAPMld, const char* sockName);
swl_rc_ne wifiGen_hapd_getConfiguredCountryCode(T_Radio* pRad, char* country, size_t countrySize);

#endif /* INCLUDE_PRIV_PLUGIN_WIFIGEN_HAPD_H_ */
