{
    "data": {
        "object": "WiFi.AccessPoint.wlan1.AssociatedDevice.2.",
        "notification": "dm:object-changed",
        "eobject": "WiFi.AccessPoint.[wlan1].AssociatedDevice.[2].",
        "parameters": {
            "AuthenticationState": {
                "from": false,
                "to": true
            },
            "SecurityModeEnabled": {
                "from": "Unknown",
                "to": "WPA2-Personal"
            }
        },
        "path": "WiFi.AccessPoint.2.AssociatedDevice.2."
    }
}
