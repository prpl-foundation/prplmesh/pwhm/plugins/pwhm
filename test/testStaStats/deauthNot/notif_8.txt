{
    "data": {
        "object": "WiFi.AccessPoint.wlan1.AssociatedDevice.1.",
        "notification": "dm:object-changed",
        "eobject": "WiFi.AccessPoint.[wlan1].AssociatedDevice.[1].",
        "parameters": {
            "AuthenticationState": {
                "from": true,
                "to": false
            },
            "DisassociationTime": {
                "from": "0001-01-01T00:00:00Z",
                "to": "2022-01-01T00:00:18Z"
            },
            "Active": {
                "from": true,
                "to": false
            },
            "LastStateChange": {
                "from": "2022-01-01T00:00:12Z",
                "to": "2022-01-01T00:00:18Z"
            }
        },
        "path": "WiFi.AccessPoint.2.AssociatedDevice.1."
    }
}
