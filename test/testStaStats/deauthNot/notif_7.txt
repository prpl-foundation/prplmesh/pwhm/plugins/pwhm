{
    "data": {
        "object": "WiFi.AccessPoint.wlan1.AssociationCount.",
        "notification": "dm:object-changed",
        "eobject": "WiFi.AccessPoint.[wlan1].AssociationCount.",
        "parameters": {
            "Disconnect": {
                "from": 1,
                "to": 2
            }
        },
        "path": "WiFi.AccessPoint.2.AssociationCount."
    }
}
